import book.Book;
import book.BooksByPagesOrAuthor;
import book.BooksByTitleOrAuthor;

import java.util.*;

/*
    Задача 1
    Используйте класс book.Book (книга) из предыдущего домашнего задания.

    Создайте компаратор, который позволит сортировать книги по названию,
    при одинаковых названиях - по автору (и там, и там - по алфавиту)

    Задача 2* (не обязательно)
    Создайте компаратор, который позволит сортировать книги по убыванию количества страниц,
    а при совпадающем объёме - по названиям ("по возрастанию", по алфавиту).
*/

public class Main {
    public static void main(String[] args) {
        Comparator<Book> comparatorByTitle = new BooksByTitleOrAuthor();
        Comparator<Book> comparatorByPages = new BooksByPagesOrAuthor();
        List<Book> books = new ArrayList<>(
            Arrays.asList(
                new Book("М.Горький", "Детство", 320),
                new Book("А.С.Пушкин", "Выстрел", 238),
                new Book("А.С.Пушкин", "Кавказский пленник", 224),
                new Book("Л.Н.Толстой", "Детство", 640),
                new Book("Л.Н.Толстой", "Кавказский пленник", 496),
                new Book("А.Н.Рыбаков", "Выстрел", 238)
            )
        );

        // Не сортированные
        for (Book book : books) {
            System.out.println(book);
        }

        // Сортированные по названию или автору
        books.sort(comparatorByTitle);
        System.out.println();
        for (Book book : books) {
            System.out.println(book);
        }
        System.out.println();

        // Сортированные по количеству страниц или автору
        books.sort(comparatorByPages);
        System.out.println();
        for (Book book : books) {
            System.out.println(book);
        }
    }
}