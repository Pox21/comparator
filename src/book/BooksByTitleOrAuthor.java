package book;

import java.util.Comparator;

public class BooksByTitleOrAuthor implements Comparator<Book> {
    private boolean isIgnoreCase = false;

    BooksByTitleOrAuthor(boolean isIgnoreCase) {
        this.isIgnoreCase = isIgnoreCase;
    }

    public BooksByTitleOrAuthor() {}

    @Override
    public int compare(Book book1, Book book2) {
        String title1 = book1.getTitle();
        String title2 = book2.getTitle();

        if (isIgnoreCase) {
           title1 = title1.toLowerCase();
           title2 = title2.toLowerCase();
        }

        if (!title1.equals(title2)){
            return title1.compareTo(title2);
        }

        return book1.getAuthor().toLowerCase().compareTo(book2.getAuthor().toLowerCase());
    }
}