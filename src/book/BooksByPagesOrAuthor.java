package book;

import java.util.Comparator;

public class BooksByPagesOrAuthor implements Comparator<Book> {

    @Override
    public int compare(Book book1, Book book2) {
        if (book2.getPages() != book1.getPages()) {
            return book2.getPages() - book1.getPages();
        }
        return book1.getAuthor().toLowerCase().compareTo(book2.getAuthor().toLowerCase());
    }
}
